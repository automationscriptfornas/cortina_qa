from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
#import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 200 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")

'''
driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
driver.get("http://192.168.1.239")'''



chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)

'''
driver=webdriver.Firefox()
driver.get("http://192.168.1.239")'''

def ENTC2117():
 driver.get(i_Host_IP_Address)
 time.sleep(5)

 driver.find_element_by_id("user").send_keys(i_user_name)
 driver.find_element_by_id("pass").send_keys(i_password)
 time.sleep(5)   

 driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()   
 time.sleep(5)   

 driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
 logger.info("Logged into Portal")
 time.sleep(10)
 
 driver.find_element_by_xpath("//div[@id='management-click']").click()
 logger.info("Clicked on the Management button")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='ntllangdiv']").click()
 logger.info("Clicked on the Name/time button")
 time.sleep(10)
 
 driver.find_element_by_xpath("//div[@id='tabs-22']//div[@id='management-details1']//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span[2]").click()
 logger.info("clicked on the edit button")
 time.sleep(5)
 
 driver.find_element_by_xpath("//input[@id='edit_name']").clear()
 logger.info("Cleared the existing name")
 time.sleep(5)
 
 driver.find_element_by_xpath("//input[@id='edit_name']").send_keys("Celestialsys")
 logger.info("Sent new name")
 time.sleep(10) 
 
 driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
 logger.info("Clicked on the OK button")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='ntllangdiv']").click()
 logger.info("once again Clicked on the Name/time button")
 time.sleep(5)

 driver.find_element_by_xpath(".//a[contains(text(), '時刻')]").click()
 logger.info("Clicked on the time button")
 time.sleep(10)
 
 driver.find_element_by_xpath("//div[@id='management-details2']//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span[text()='編集'][1]").click()
 logger.info("clicked on the edit button")
 time.sleep(5)
 
 driver.find_element_by_xpath("//input[@id='edit_disable']").click()
 logger.info("CLicked on the radio button")
 time.sleep(5)

 driver.find_element_by_xpath("//input[@id='dis-hr']").clear()
 logger.info("Cleared older value in the select box")
 time.sleep(5)

 driver.find_element_by_xpath("//input[@id='dis-hr']").send_keys("16")
 logger.info("Send some keys")
 time.sleep(5)

 driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
 logger.info("Clicked on the OK button")
 time.sleep(10)
 
 driver.find_element_by_xpath("//a[@id='management-back']").click()
 logger.info("Clicked on the TOP button")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='filesharing-click']").click()
 logger.info("Clicked on the file sharing")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='smblan']").click()
 logger.info("Clicked on the smb protocol")
 time.sleep(5)

 if(driver.find_element_by_xpath("//input[@id='enabled_smb']").is_selected()):
 	driver.find_element_by_xpath("//input[@id='disabled_smb']").click()
 	time.sleep(5)/
 else:
 	driver.find_element_by_xpath("//input[@id='enabled_smb']").click()
 	time.sleep(5)
 driver.find_element_by_xpath("//button[@id='confirm_smb']").click()
 time.sleep(5)

Report_Generation.AddRow(i_HTML_Report_name, 'Management_ENTC_2117', 'Management Name/time', 'Pass')
try:    
    ENTC2117()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'Management_ENTC_2117', 'Management Name/time', 'Fail')
    logger.info("Exception ::    " + str(e))
logger.info("Drives ENTC209 Test case is successfully Ended")
driver.close()