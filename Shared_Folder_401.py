from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
from Reporting import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Shared Folder ENTC 401 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")

driver=webdriver.Firefox()


def ENTC401():
    driver.get(i_Host_IP_Address)
    time.sleep(5)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    time.sleep(4)   
    
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()   
    time.sleep(3)   
    
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    logger.info("Logged into Portal")
    time.sleep(8)
     
    driver.find_element_by_xpath("//div[@id='filesharing-click']").click()
    logger.info("Clicked File sharing")
    time.sleep(5)
    
    driver.find_element_by_xpath("//div[@id='folsetlan']").click()
    logger.info("Clicked Shared Folders")
    time.sleep(5)
    
    
    if (driver.find_element_by_xpath("//tr[@pq-row-indx='0']").is_displayed()):
        logger.info("Shared Folder list exists")
        Report_Generation.AddRow(i_HTML_Report_name, 'Shared Folder 401', 'Lists existing shared folders', 'Pass')
    else:
        logger.info("Shared Folder list does not exists")
        Report_Generation.AddRow(i_HTML_Report_name, 'Shared Folder 401', 'Lists existing shared folders', 'Fail')
    
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']").click()
    logger.info("Closing the Shared Folder list window")
    
    time.sleep(4)
    driver.find_element_by_xpath("//a[@id='filesharing-back']").click()
    time.sleep(4)
    
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    logger.info("Logout of the session")
    time.sleep(4)
    

try:    
    ENTC401()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'Shared Folder 401', 'Lists existing shared folders', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("Shared Folder 401 Test case is successfully Ended")
driver.close()