from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 200 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")

'''
driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
driver.get("http://192.168.1.239")'''



chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)

'''
driver=webdriver.Firefox()
driver.get("http://192.168.1.239")'''

def ENTC1727():
 time.sleep(20)
 driver.get(i_Host_IP_Address)
 time.sleep(5)

 driver.find_element_by_id("user").send_keys(i_user_name)
 driver.find_element_by_id("pass").send_keys(i_password)
 time.sleep(5)   

 driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()   
 time.sleep(5)   

 driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
 logger.info("Logged into Portal")
 time.sleep(10)
 
 driver.find_element_by_xpath("//div[@id='filesharing-click']").click()
 logger.info("Clicked File sharing")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='grplan']").click()
 logger.info("Clicked on group")
 time.sleep(5)

 driver.find_element_by_xpath("//span[text()='グループの作成']").click()
 logger.info("Clicked on the create group")
 time.sleep(5)

 driver.find_element_by_xpath("//input[@id='group_name']").send_keys("Manjunath")
 logger.info("Clicked on the group name text box")
 time.sleep(5)
 
 driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()b
 logger.info("Clicked on the OK button")
 time.sleep(5)

 Report_Generation.AddRow(i_HTML_Report_name, 'User_Group_ENTC_600', 'Group creation', 'Pass')
try:    
    ENTC505()
except Exception as e:
	print("Test for html name::" + i_HTML_Report_name)
    Report_Generation.AddRow(i_HTML_Report_name, 'User_Group_ENTC_600', 'Group creation', 'Fail')
    logger.info("Exception ::    " + str(e))
logger.info("Drives ENTC209 Test case is successfully Ended")
driver.close()