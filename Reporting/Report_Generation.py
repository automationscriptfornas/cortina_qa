import HTML
import sys
import logging
import inspect
import urllib
from _datetime import datetime

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('F:\outputfortest.txt')
logger.addHandler(fh)
output_line = None
l_firmware = None

def CreateFile(i_file_name,i_firmware):
    with open(i_file_name, 'w') as ReportFile:
        t = None
        t = HTML.Table(header_row=['Test Case Number',   'Test case Description',   'Result', 'Time'])
        htmlcode = "<html><body><tr><td style=color:red><b> Firmware :: "  + i_firmware + "</b></td></tr>" + str(t) + "</body><html>"
        ReportFile.write(htmlcode)

def AddRow(i_file_name,i_test_case_no,i_test_case_Desc,i_result):
        output_line = None
        f = open(i_file_name.replace("']", ""), "r").read()
        line = str(f)
        index = line.find('</TABLE></body><html>')
        if (i_result == 'Pass'):
            output_line = line[:index] + '<tr><td style="color:green">' + i_test_case_no + '</td><td style="color:green">'+ i_test_case_Desc + '</td><td style="color:green">' + i_result + '</td><td style="color:green">' + datetime.now().strftime("%A, %d. %B %Y %I:%M%p") +'</td></tr>' + line[index:]
        if (i_result == 'Fail'):
            output_line = line[:index] + '<tr><td style="color:red">' + i_test_case_no + '</td><td style="color:red">'+ i_test_case_Desc + '</td><td style="color:red">' + i_result + '</td><td style="color:red">' + datetime.now().strftime("%A, %d. %B %Y %I:%M%p") +'</td></tr>' + line[index:]
                
        #logger.info(str(output_line))
        with open(i_file_name.replace("']", ""), 'w') as ReportFile1:   
            ReportFile1.write(output_line) 
#===============================================================================
# 
# def cerate_table():
#     t = HTML.Table(header_row=['Test Case Number',   'Test case Description',   'Result'])
#     
# def update_row():
#      t.rows.append(str(sys.argv[1]),str(sys.argv[2]),str(sys.argv[3]))
#      
# def html_output():
#     htmlcode = str(t)
#===============================================================================
     