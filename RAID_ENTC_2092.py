from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import time
import logging
import sys
from Reporting import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[0].replace("['", ""))

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)



driver = webdriver.Firefox()
fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("RAID ENTC 2092 Script Start")
logger.info("")


def delete0():
	driver = webdriver.Firefox()

	driver.get(i_Host_IP_Address)
	time.sleep(3)

	driver.find_element_by_id("user").send_keys("admin")
	driver.find_element_by_id("pass").send_keys("admin")
	time.sleep(1)
	driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
	time.sleep(1)
	driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
	time.sleep(1)

	driver.find_element_by_id("drives-click").click()
	time.sleep(1)

	driver.find_element_by_id("raid-div").click()
	time.sleep(7)

	driver.find_element_by_link_text("Array1").click()
	time.sleep(2)

	driver.find_element_by_xpath("//span[text()='Delete Raid Array']").click()
	time.sleep(3)

	driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button//span[text()='Confirm']").click()
	time.sleep(2)

	text= driver.find_element_by_id("lvmvalidnum").text
	driver.find_element_by_id("lvmtext-add").send_keys(text)
	time.sleep(6)
	driver.find_element_by_id("id_of_button").click()
	time.sleep(15)
	driver.close()
	Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2092', 'RAID Deletion', 'Pass')
	
try:	
	delete0()
except Exception as e:
	Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2092', 'RAID Deletion', 'Fail')
	logger.info("Exception ::   " + str(e))
	

logger.info("RAID ENTC 2092 Script End")
driver.close()


