from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 200 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")

'''
driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
driver.get("http://192.168.1.239")'''



chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)

'''
driver=webdriver.Firefox()
driver.get("http://192.168.1.239")'''

def ENTC1727():
 time.sleep(20)
 driver.get(i_Host_IP_Address)
 time.sleep(5)

 driver.find_element_by_id("user").send_keys(i_user_name)
 driver.find_element_by_id("pass").send_keys(i_password)
 time.sleep(5)   

 driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()   
 time.sleep(5)   

 driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
 logger.info("Logged into Portal")
 time.sleep(10)
 
 driver.find_element_by_xpath("//div[@id='filesharing-click']").click()
 logger.info("Clicked File sharing")
 time.sleep(5)

 driver.find_element_by_xpath("//div[@id='uselan']").click()
 logger.info("Clicked User")
 time.sleep(5)

 driver.find_element_by_xpath("//span[text()='ユーザーの作成']").click()
 logger.info("Clicked on create user")
 time.sleep(15)
 
 driver.find_element_by_xpath("//input[@id='un']").send_keys("Manjunath3")
 logger.info("User name passed")
 time.sleep(5)

 driver.find_element_by_xpath("//input[@id='pass1']").send_keys("Manjuanth")
 logger.info("Password is supplied")
 time.sleep(5)
 
 driver.find_element_by_xpath("//input[@id='pwd']").send_keys("Manjuanth")
 logger.info("Confirmation password is passed")
 time.sleep(5)

 driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
 logger.info("OK is clicked")
 time.sleep(5)

 '''driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle']//button//span[1]").click()
 logger.info("Close button is clicked")
 time.sleep(5)'''
 print("Test for html name::" + i_HTML_Report_name)
 Report_Generation.AddRow(i_HTML_Report_name, 'User_Group_ENTC_1727', 'User Creation', 'PASS')

try:    
    ENTC1727()
except Exception as e:
 print("Test for html name::" + i_HTML_Report_name)
 Report_Generation.AddRow(i_HTML_Report_name, 'User_Group_ENTC_1727', 'User Creation', 'Fail')
 logger.info("Exception ::    " + str(e))

logger.info("Drives ENTC209 Test case is successfully Ended")
driver.close()