from xlrd import open_workbook
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import time
import logging
import os
import HTML
import subprocess
import sys

logger_Test_Run = logging.getLogger("Smoke TEST RUN")
logger_Test_Run.setLevel(logging.DEBUG)
fh1 = logging.FileHandler("F:\Smoke_Test_Run.txt")
fh1.setLevel(logging.DEBUG)
formatter1 = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh1.setFormatter(formatter1)
logger_Test_Run.addHandler(fh1)

logger_Test_Run.info("================")
logger_Test_Run.info("Smoke Test Script Execution Started ::")
logger_Test_Run.info("")



wb = open_workbook('Book2.xls')
values = []
for s in wb.sheet_by_index(0):
    #print 'Sheet:',s.name
    for row in range(1, s.nrows):
        col_names = s.row(0)
        col_value = []
        for name, col in zip(col_names, range(s.ncols)):
            value  = (s.cell(row,col).value)
            try : 
                value = str(int(value))
                print(value)
            except : pass
            col_value.append((name.value, value))
        values.append(col_value)
print values


logger_Test_Run.info("Starting the Script Drives_ENTC_200")
import Drives_ENTC__200
logger_Test_Run.info("End of Execution for Script Drives_ENTC_200")

logger_Test_Run.info("Starting the Script Drives_ENTC_208")
import Drives_ENTC__208
logger_Test_Run.info("End of Execution for Script Drives_ENTC_208")

logger_Test_Run.info("Starting the Script RAID_ENTC_2091")
import RAID_ENTC_2091
logger_Test_Run.info("End of Execution for Script RAID_ENTC_2091")

logger_Test_Run.info("Starting the Script RAID_ENTC_2093")
import RAID_ENTC_2093
logger_Test_Run.info("End of Execution for Script RAID_ENTC_2093")

logger_Test_Run.info("Starting the Script RAID_ENTC_2089")
import RAID_ENTC_2089
logger_Test_Run.info("End of Execution for Script RAID_ENTC_2089")

logger_Test_Run.info("Starting the Script RAID_ENTC_2093")
import RAID_ENTC_2093
logger_Test_Run.info("End of Execution for Script RAID_ENTC_2093")


#User can import files in the order,they wants to run scripts. top down approach 