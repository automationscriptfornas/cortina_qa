from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import logging
import sys
from Reporting import Report_Generation

# Creating Logger instance to log events of this particular test case
logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Notifications ENTC 140 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")
driver = webdriver.Firefox()


# One single function is being created and this will be called at the end of this file, this will help us call this test script from other test scripts
def ENTC140():
    
    #driver.get("http://192.168.1.84")
    driver.get(i_Host_IP_Address)
    
    time.sleep(5)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    
    time.sleep(4)
    
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    
    time.sleep(3)
    
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    logger.info("Logged into Portal")
    time.sleep(5)
    
    driver.find_elements_by_xpath("//div[@id='management-click']")[0].click()
    logger.info("Management Click Successful")
    time.sleep(5)

    driver.find_elements_by_xpath("//div[@id='notiflangdiv']")[0].click()
    logger.info("Notifications Click Successful")
    time.sleep(10)

    if (driver.find_element_by_xpath("//li[@class='ui-state-default ui-corner-top ui-tabs-active ui-state-active']").is_displayed()):
        logger.info("Alert Panel exists")
    else:
        logger.info("Failure :: Alert Panel does not exist")

    if (driver.find_element_by_xpath("//li[@class='ui-state-default ui-corner-top']").is_displayed()):
        logger.info("Display Panel exists")
    else:
        logger.info("Failure :: Display Panel does not exist")
    
    time.sleep(20)    
    driver.find_element_by_xpath("//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']").click()
    logger.info("Closing the status notification window")
    
    time.sleep(5)

    driver.find_element_by_xpath("//a[@id='management-back']").click()
    
    time.sleep(4)
    
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    logger.info("Logout of the session")
    time.sleep(4)
    Report_Generation.AddRow(i_HTML_Report_name, 'Notifications_ENTC_140', 'Notification Main Page Settings', 'Pass')


try:    
    ENTC140()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'Notifications_ENTC_140', 'Notification Main Page Settings', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("Notifications_ENTC_140 Script End")
driver.close()    
         
        