
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
import time
import logging
import sys
from Reporting import Report_Generation

#driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
driver = webdriver.Firefox()

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[0].replace("['", ""))

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)


fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("RAID ENTC 2093 Script Start")
logger.info("")

def create2():
    #driver.get("http://192.168.1.84")
    driver.get(i_Host_IP_Address)
    time.sleep(3)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    time.sleep(3)
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    time.sleep(3)
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    time.sleep(4)
    
    logger.info("Logged into Portal")
    driver.find_element_by_id("drives-click").click()
    logger.info("Clicked on drives ")
    time.sleep(1)
    
    
    driver.find_element_by_id("raid-div").click()
    logger.info("Clicked RAID module link")
    time.sleep(20)
    
    driver.find_element_by_link_text("Array1").click()
    logger.info("Clicked Array 1 Link")
    time.sleep(20)
    
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]").click()
    logger.info("Clicked Delete Array Button")
    time.sleep(10)
    
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button[1]").click()
    #driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]//*[contains(text(), 'OK')]").click()
    #driver.find_element_by_xpath("//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
    #driver.find_element_by_css_selector("input[type='div'][aria-labelledby='ui-id-3']").find_element_by_xpath("//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
    logger.info("Clicked Confirm button on delete")
    time.sleep(2)
    
    text= driver.find_element_by_id("lvmvalidnum").text
    logger.info("Got Captcha Text")
    driver.find_element_by_id("lvmtext-add").send_keys(text)
    logger.info("Enter Captcha TExt into Captcha popup text box")
    time.sleep(6)
    driver.find_element_by_id("id_of_button").click()
    logger.info("Clicked on OK button of captcha window")
    Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2093', 'RAID 1 Deletion', 'Pass')
    time.sleep(30)

try:    
    create2()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2093', 'RAID 1 Deletion', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("RAID ENTC 2093 Script End")
driver.close()