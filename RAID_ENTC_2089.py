from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import time
import logging
import sys
from Reporting import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[0].replace("['", ""))

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)


#driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
driver = webdriver.Firefox()
fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("RAID ENTC 2089 Script Start")
logger.info("")


def create1():
    

    #driver.get("http://192.168.1.84")
    driver.get(i_Host_IP_Address)
    time.sleep(3)

    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    time.sleep(1)
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    time.sleep(1)
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    time.sleep(1)

    logger.info("Logged into Portal")
    driver.find_element_by_id("drives-click").click()
    time.sleep(20)
    logger.info("Clicked on Main Drives link")

    
    driver.find_element_by_id("raid-div").click()
    logger.info("Clicked on RAID link")
    
    time.sleep(30)
    driver.find_element_by_link_text("Array1").click()
    time.sleep(3)
    logger.info("Clicked on Array1 link")

    #driver.find_element_by_css_selector("select#raid_mode > option[value='1']").click()
    #driver.find_element_by_xpath("//select[@id='raid_mode']/option[text()='0']").click()
    driver.find_element_by_css_selector("select#raid_mode > option[value='0']").click()
    time.sleep(20)
    logger.info("Selected RAID 1 in dropdown")
    
    #driver.find_element_by_id("rync_enabled").click()
    #logger.info("Selected rsync enabled")
    driver.find_element_by_xpath("//div[@class='pq-header-outer ui-widget-header']//th[@class='pq-grid-col  pq-align-center pq-last-frozen-col pq-left-col ui-state-default pq-grid-col-leaf']//input[@type='checkbox']").click()
    time.sleep(10)
    logger.info("Clicked on select all checkbox")
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]").click()
    
    logger.info("Clicked on create array")
    time.sleep(10)
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button//span[text()='OK']").click()
    #driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//*[contains(text(), 'OK')]").click()
    #driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//*[contains(text(), 'OK')]").click()
    logger.info("Clicked on confirm create array button")
    time.sleep(2)
    text= driver.find_element_by_id("lvmvalidnum").text
    logger.info("Get Captcha Text")
    driver.find_element_by_id("lvmtext-add").send_keys(text)
    logger.info("Enter Captcha text")
    time.sleep(6)
    driver.find_element_by_id("id_of_button").click()
    logger.info("Clicked on OK button of captcha popup")
    time.sleep(180)
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//span[text()='Ok']").click()
    logger.info("Clicked on OK button of Successful RAID creation popup")
    
    time.sleep(4)
    
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2089', 'RAID 0 creation with 2 drives', 'Pass')
    logger.info("Logout of the session")
    time.sleep(4)

    
try:    
    create1()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'RAID_ENTC_2089', 'RAID 0 creation with 2 drives', 'Fail')
    logger.info("Exception ::    " + str(e))

logger.info("RAID ENTC 2089 Script End")
driver.close()
#print " successfully created array1 with raid 1"
#driver.find_element_by_xpath("//span[text()='Delete Raid Array']").click()
#time.sleep(3)

#driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button//span[text()='Confirm']").click()
#time.sleep(2)

#text= driver.find_element_by_id("lvmvalidnum").text
#driver.find_element_by_id("lvmtext-add").send_keys(text)
#time.sleep(6)
#driver.find_element_by_id("id_of_button").click()
#time.sleep(15)
#driver.refresh()