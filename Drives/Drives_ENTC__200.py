from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import logging
import sys
from Reporting import Report_Generation

# Creating Logger instance to log events of this particular test case
logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 200 Script Start")
logger.info("")
driver = webdriver.Firefox()


# One single function is being created and this will be called at the end of this file, this will help us call this test script from other test scripts
def ENTC200():
    
    #driver.get("http://192.168.1.84")
    driver.get(i_Host_IP_Address)
    
    time.sleep(5)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    
    time.sleep(4)
    
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    
    time.sleep(3)
    
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    logger.info("Logged into Portal")
    time.sleep(5)
    
    driver.find_elements_by_xpath("//div[@id='drives-click']")[0].click()
    logger.info("Drive Click Successful")
    time.sleep(5)
    
    driver.find_element_by_xpath("//div[@id='drive-ref']").click()
    logger.info("Click on Drives inside main drive module Successful")
    time.sleep(60)
    
     
    
    
    
    if (driver.find_element_by_id("driver_disk_array_DISK1").is_displayed()):
        logger.info("Drive list exists")
    else:
        logger.info("Failure :: Drive list does not exist")
        
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']").click()
    logger.info("Closing the drive list window")
    
    time.sleep(4)
    
    driver.find_element_by_xpath("//a[@id='drives-back']").click()
    
    time.sleep(4)
    
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    logger.info("Logout of the session")
    time.sleep(4)
    Report_Generation.AddRow(i_HTML_Report_name, 'Drive_ENTC_200', 'Display all the drives details', 'Pass')

# Calling the Main function in this test script
try:    
    ENTC200()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'Drive_ENTC_200', 'Display all the drives details', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("Drives ENTC 200 Script End")
driver.close()
    