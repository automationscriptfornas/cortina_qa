from selenium import webdriver
import time
import os
import logging
import sys
from Reporting import Report_Generation

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[0].replace("['", ""))

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)


fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 208 Script Start")
logger.info("")
driver = webdriver.Firefox()



#driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
def create4():
    driver.get(i_Host_IP_Address)
    
    '''chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
    os.environ["webdriver.chrome.driver"] = chromedriver
    driver = webdriver.Chrome(chromedriver)
    driver.get("http://192.168.1.179")'''
    
    time.sleep(5)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    time.sleep(3)
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    time.sleep(3)
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    time.sleep(3)
    logger.info("Logged into Portal")
    
    driver.find_elements_by_xpath("//div[@id='drives-click']")[0].click()
    time.sleep(1)
    logger.info("Clicked on Main Drives Page")
    
    driver.find_element_by_xpath("//div[@id='drive-ref']").click()
    time.sleep(10)
    logger.info("Click on Drives inside main drive module Successful")
    
    driver.find_element_by_id("driver_disk_array_DISK1").click()
    time.sleep(5)
    logger.info("Clicked on Disk1")
    
    #driver.find_element_by_xpath("//span[text()='Format Disk']").click()
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//div[@class='ui-dialog-content ui-widget-content']//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][2]").click()
    time.sleep(1)
    logger.info("Clicked on Format Disk")
    
    #driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='Confirm']").click()
    time.sleep(5)
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div[@class='ui-dialog-buttonset']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]").click()
    logger.info("Clicked on OK button")
    
    #driver.find_elements_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='Confirm']").click()
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[1]").click()
    #//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]
    time.sleep(10)
    
    text= driver.find_element_by_id("lvmvalidnum").text
    
    driver.find_element_by_id("lvmtext-add").send_keys(text)
    
    time.sleep(6)
    
    driver.find_element_by_id("id_of_button").click()
    
    time.sleep(40)
    
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//span[text()='Ok']").click()
    time.sleep(20)
    
    driver.find_element_by_id("driver_disk_array_DISK2").click()
    time.sleep(4)
    logger.info("Clicked on Disk2")
    
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//div[@class='ui-dialog-content ui-widget-content']//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][2]").click()
    time.sleep(1)
    logger.info("Clicked on Format Disk")
    
    #driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='Confirm']").click()
    time.sleep(5)
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div[@class='ui-dialog-buttonset']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]").click()
    logger.info("Clicked on OK button")
    
    time.sleep(5)
    #driver.find_elements_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='Confirm']").click()
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[1]").click()
    #//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]
    time.sleep(10)
    
    text= driver.find_element_by_id("lvmvalidnum").text
    
    driver.find_element_by_id("lvmtext-add").send_keys(text)
    
    time.sleep(6)
    
    driver.find_element_by_id("id_of_button").click()
    
    time.sleep(40)
    
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//span[text()='Ok']").click()
    time.sleep(10)
    
    
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']").click()
    logger.info("Closing the drive list window")
        
    time.sleep(5)
        
    driver.find_element_by_xpath("//a[@id='drives-back']").click()
        
    time.sleep(5)
        
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    logger.info("Logout of the session")
    print ("inside main func " + i_HTML_Report_name)
    Report_Generation.AddRow(i_HTML_Report_name, 'Drive_ENTC_208', 'Disk format for individual drives', 'Pass')
    time.sleep(4)

try:    
    create4()
except Exception as e:
    print ("inside try func " + i_HTML_Report_name)
    Report_Generation.AddRow(i_HTML_Report_name, 'Drive_ENTC_208', 'Disk format for individual drives', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("End of Drives ENTC 208 Script")
driver.close()
